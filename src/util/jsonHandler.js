require("dotenv").config();

// None of these need to be npm installed as they come with Node.js.
import util from "util";
import fs from "fs";
import path from "path";

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const catsPath = path.resolve(process.env.catsDatabase);

async function readCats() {
  const json = await readFile(catsPath);
  return JSON.parse(json);
}

async function writeCats(cats) {
  const json = JSON.stringify(cats);
  return writeFile(catsPath, json);
}

export { readCats, writeCats };
